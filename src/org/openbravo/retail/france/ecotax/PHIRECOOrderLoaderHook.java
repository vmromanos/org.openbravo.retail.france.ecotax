/*
 ************************************************************************************
 * Copyright (C) 2020 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package org.openbravo.retail.france.ecotax;

import java.math.BigDecimal;

import javax.enterprise.context.ApplicationScoped;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.france.ecotax.PhiecoTaxcategory;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.retail.posterminal.OrderLoaderHook;

@ApplicationScoped
public class PHIRECOOrderLoaderHook implements OrderLoaderHook {

  @Override
  public void exec(JSONObject jsonorder, Order order, ShipmentInOut shipment, Invoice invoice)
      throws Exception {

    for (OrderLine orderline : order.getOrderLineList()) {
      if (orderline.getProduct() != null) {
        PhiecoTaxcategory deee = orderline.getProduct().getPhiecoDeee();
        PhiecoTaxcategory dea = orderline.getProduct().getPhiecoDea();

        BigDecimal deeeamnt = orderline.getProduct().getPhiecoDeeeamnt();
        if (deeeamnt == null) {
          if (deee != null && deee.getTaxamt() != null) {
            BigDecimal deeeQty = orderline.getProduct().getPhiecoDeeeQty();
            deeeamnt = deee.getTaxamt().multiply(deeeQty);
          } else {
            deeeamnt = BigDecimal.ZERO;
          }
        }

        // regular, code-based ecotaxes are used tax not included:
        // compute the net first, and then the gross from it
        orderline.setPhiecoDeeenet(deeeamnt.multiply(orderline.getOrderedQuantity()));
        BigDecimal taxAmount = orderline.getPhiecoDeeenet()
            .multiply(orderline.getTax().getRate())
            .divide(new BigDecimal("100"));
        orderline.setPhiecoDeeegross(orderline.getPhiecoDeeenet().add(taxAmount));

        BigDecimal deaamnt = orderline.getProduct().getPhiecoDeaamnt();
        if (deaamnt == null) {
          if (dea != null && dea.getTaxamt() != null) {
            BigDecimal deaQty = orderline.getProduct().getPhiecoDeaQty();
            deaamnt = dea.getTaxamt().multiply(deaQty);
          } else {
            deaamnt = BigDecimal.ZERO;
          }
        }

        // regular, code-based ecotaxes are used tax not included:
        // compute the net first, and then the gross from it
        orderline.setPhiecoDeanet(deaamnt.multiply(orderline.getOrderedQuantity()));
        taxAmount = orderline.getPhiecoDeanet()
            .multiply(orderline.getTax().getRate())
            .divide(new BigDecimal("100"));
        orderline.setPhiecoDeagross(orderline.getPhiecoDeanet().add(taxAmount));

        orderline.setPhiecoDeee(deee);
        orderline.setPhiecoDea(dea);
      }
    }

    if (invoice != null && !invoice.getInvoiceLineList().isEmpty()) {
      for (InvoiceLine invoiceLine : invoice.getInvoiceLineList()) {
        if (invoiceLine.getProduct() != null) {
          invoiceLine.setPhiecoDeeenet(invoiceLine.getProduct()
              .getPhiecoDeeeamnt()
              .multiply(invoiceLine.getInvoicedQuantity()));
          invoiceLine.setPhiecoDeanet(invoiceLine.getProduct()
              .getPhiecoDeaamnt()
              .multiply(invoiceLine.getInvoicedQuantity()));
          invoiceLine.setPhiecoDeeegross(invoiceLine.getPhiecoDeeenet()
              .add(invoiceLine.getPhiecoDeeenet()
                  .multiply(invoiceLine.getTax().getRate())
                  .divide(new BigDecimal("100"))));
          invoiceLine.setPhiecoDeagross(invoiceLine.getPhiecoDeanet()
              .add(invoiceLine.getPhiecoDeanet()
                  .multiply(invoiceLine.getTax().getRate())
                  .divide(new BigDecimal("100"))));
          invoiceLine.setPhiecoDeee(invoiceLine.getProduct().getPhiecoDeee());
          invoiceLine.setPhiecoDea(invoiceLine.getProduct().getPhiecoDea());
        }
      }
    }

  }

}
